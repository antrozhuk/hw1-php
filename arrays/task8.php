<?php
echo '<a href="/arrays">back</a><br>';
function halfRev($arr){
if(count($arr)%2==0){
    for($i=0;$i<count($arr)/2;$i++)
    {
        $a=$arr[$i];
        $arr[$i]=$arr[$i+count($arr)/2];
        $arr[$i+count($arr)/2]=$a;
    }
}
else
{
    for($i=0;$i<(count($arr)-1)/2;$i++)
    {
        $a=$arr[$i];
        $arr[$i]=$arr[$i+(count($arr)+1)/2];
        $arr[$i+(count($arr)+1)/2]=$a;
    }
}
return $arr;
}
echo "Половинный реверс массива <br>[1,2,3,4,5]=>[ " ;
foreach (halfRev([1,2,3,4,5]) as $key => $val) {
    echo "$val ";
}
echo "]";
echo "<br>[1,2,3,4,5,6]=>[ " ; 
foreach (halfRev([1,2,3,4,5,6]) as $key => $val) {
    echo "$val ";
}
echo "]";